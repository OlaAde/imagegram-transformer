const AWS = require('aws-sdk');
const { handler } = require('./index');

// Mock the AWS.S3 getObject method
jest.mock('aws-sdk', () => {
    const mockGetObject = jest.fn().mockImplementation((params) => {
        // Simulate successful retrieval of original image from S3
        const data = {
            Body: Buffer.from('Original Image Data'),
        };
        return {
            promise: () => Promise.resolve(data),
        };
    });

    return {
        S3: jest.fn(() => ({
            getObject: mockGetObject,
            putObject: jest.fn().mockReturnValue({ promise: () => Promise.resolve({ ETag: '12345' }) }),
        })),
    };
});

// Mock the MongoDB updateOne method
jest.mock('mongodb', () => ({
    MongoClient: {
        connect: jest.fn().mockResolvedValue({
            db: jest.fn().mockReturnThis(),
            collection: jest.fn().mockReturnThis(),
            updateOne: jest.fn().mockResolvedValue(),
        }),
    },
}));

describe('Image Processing Lambda', () => {
    it('should process and upload image successfully', async () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'example.jpg',
                        },
                    },
                },
            ],
        };

        const result = await handler(event);

        expect(result).toEqual('Image processing and upload successful.');
    });

    it('should handle image processing and upload failure', async () => {
        const event = {
            Records: [
                {
                    s3: {
                        object: {
                            key: 'example.jpg',
                        },
                    },
                },
            ],
        };

        // Mock the AWS.S3 getObject to throw an error
        AWS.S3.prototype.getObject.mockImplementation(() => {
            throw new Error('Error retrieving image');
        });

        // Mock the MongoDB updateOne to throw an error
        jest.mock('mongodb', () => ({
            MongoClient: {
                connect: jest.fn().mockResolvedValue({
                    db: jest.fn().mockReturnThis(),
                    collection: jest.fn().mockReturnThis(),
                    updateOne: jest.fn().mockRejectedValue(),
                }),
            },
        }));

        await expect(handler(event)).rejects.toThrow('Image processing and upload failed.');
    });
});
