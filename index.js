const AWS = require('aws-sdk');
const sharp = require('sharp');
const MongoClient = require('mongodb').MongoClient;

const s3 = new AWS.S3();
const mongoUri = process.env.MONG0DB_URI;
const originalBucketName = process.env.ORIGINAL_BUCKET_NAME;
const convertedBucketName = process.env.CONVERTED_BUCKET_NAME;

exports.handler = async (event) => {
    try {
        const {key} = event.Records[0].s3.object;
        const postId = key.substring(0, key.lastIndexOf('.'));

        const originalImage = await s3.getObject({Bucket: originalBucketName, Key: key}).promise();

        const convertedImage = await sharp(originalImage.Body).resize(600, 600).toFormat('jpeg').toBuffer();

        const uploadParams = {Bucket: convertedBucketName, Key: key, Body: convertedImage};
        await s3.putObject(uploadParams).promise();

        const updateParams = {imageStatus: 'READY_TO_SERVE'};
        await updatePostImageStatus(postId, updateParams);

        return 'Image processing and upload successful.';
    } catch (error) {
        console.error('Error:', error);

        const postId = key.substring(0, key.lastIndexOf('.'));
        const updateParams = {imageStatus: 'CONVERTED_IMAGE_UPLOAD_FAILED'};
        await updatePostImageStatus(postId, updateParams);

        throw new Error('Image processing and upload failed.');
    }
};

async function updatePostImageStatus(postId, updateParams) {
    const client = await MongoClient.connect(mongoUri, {useNewUrlParser: true});
    const db = client.db();
    const collection = db.collection('posts');

    await collection.updateOne({_id: postId}, {$set: updateParams});

    client.close();
}
